# simple-go

A simple go project to demonstrate exchanging data with Flow Cloud

## Usage

1. Set `FLOW_CLOUD_NODE_ID` and `FLOW_CLOUD_NODE_PASSWORD` to the appropriate values for your node
   - linux/macos: `export FLOW_CLOUD_NODE_ID={value}` `export FLOW_CLOUD_NODE_PASSWORD={value}`
   - windows: `set "FLOW_CLOUD_NODE_ID={value}"` `set "FLOW_CLOUD_NODE_PASSWORD={value}"`
2. Run the program:
   - `go run main.go`
   1. `engine/temp` and `gearbox/pressure` will automatically at a rate determined by `updateInterval`
   2. `position/latitude` and `position/longitude` can be adjusted using the arrow keys
   3. `cabin/temp` can be adjusted using +/- keys
   4. Exit using `q`, `ESC` or `ctrl-C`
