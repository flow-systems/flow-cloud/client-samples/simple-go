module gitlab.com/flow-systems/flow-cloud/client-samples/simple-go

go 1.17

require github.com/eclipse/paho.mqtt.golang v1.3.5

require (
	github.com/eiannone/keyboard v0.0.0-20200508000154-caf4b762e807 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0 // indirect
	golang.org/x/sys v0.0.0-20200323222414-85ca7c5b95cd // indirect
)
