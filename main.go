package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/eiannone/keyboard"
)

var rnd = rand.New(rand.NewSource(time.Now().UnixNano()))

type measurement struct {
	topic string
	value float64
}

func main() {
	// Update interval for engineTemp and gearboxPress
	const updateInterval = 5 * time.Second

	// Get the environment variables
	nodeID := os.Getenv("FLOW_CLOUD_NODE_ID")
	if nodeID == "" {
		panic("Environment variable FLOW_CLOUD_NODE_ID is not set")
	}
	nodePassword := os.Getenv("FLOW_CLOUD_NODE_PASSWORD")
	if nodePassword == "" {
		panic("Environment variable FLOW_CLOUD_NODE_PASSWORD is not set")
	}
	broker := os.Getenv("FLOW_CLOUD_BROKER")
	if broker == "" {
		broker = "tcps://mqtt.flow-cloud.io:8883"
	}

	// Create an MQTT client
	opts := mqtt.NewClientOptions().
		AddBroker(broker).
		SetUsername(nodeID).
		SetPassword(nodePassword)
	// opts.SetDefaultPublishHandler(messagePubHandler)
	// opts.OnConnect = connectHandler
	// opts.OnConnectionLost = connectLostHandler
	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	fmt.Printf("Starting simulator. Press: \n\t+/i to adjust cabin/temp\n\tarrow keys to adjust position\n\tq, ESC or ctrl-C to quit\n")

	// Create a timer to regularly update engine/gearbox channels
	ticker := time.NewTicker(updateInterval)

	// Get the keyboard input
	keysEvents, err := keyboard.GetKeys(10)
	if err != nil {
		panic(err)
	}

	// Signal when done
	done := make(chan bool)

	defer func() {
		fmt.Println("Closing keyboard")
		_ = keyboard.Close()
	}()

	// Create and publish the starting values
	// Initial dashboard values
	var (
		engineTemp   = measurement{"engine/temp", 90.0}
		gearboxPress = measurement{"gearbox/pressure", 3.4}
		cabinTemp    = measurement{"cabin/temperature", 20.0}
		latitude     = measurement{"position/latitude", 51.509261}
		longitude    = measurement{"position/longitude", -0.232396}
	)

	publish(client, nodeID, engineTemp)
	publish(client, nodeID, gearboxPress)
	publish(client, nodeID, cabinTemp)
	publish(client, nodeID, latitude)
	publish(client, nodeID, longitude)

	go func() {
		for {
			select {
			case <-ticker.C:
				engineTemp.value = nextValue(engineTemp.value, 2.0)
				publish(client, nodeID, engineTemp)
				gearboxPress.value = nextValue(gearboxPress.value, 0.5)
				publish(client, nodeID, gearboxPress)
			case event := <-keysEvents:
				if event.Err != nil {
					panic(event.Err)
				}
				// fmt.Printf("You pressed: rune %q, key %X\r\n", event.Rune, event.Key)
				switch event.Key {
				case keyboard.KeyEsc, keyboard.KeyCtrlC:
					done <- true
				case keyboard.KeyArrowUp:
					latitude.value += 0.001
					publish(client, nodeID, latitude)
				case keyboard.KeyArrowDown:
					latitude.value -= 0.001
					publish(client, nodeID, latitude)
				case keyboard.KeyArrowLeft:
					longitude.value -= 0.001
					publish(client, nodeID, longitude)
				case keyboard.KeyArrowRight:
					longitude.value += 0.001
					publish(client, nodeID, longitude)
				case 0:
					switch event.Rune {
					case 'q':
						done <- true
					case '+':
						cabinTemp.value += 1.0
						publish(client, nodeID, cabinTemp)
					case '-':
						cabinTemp.value -= 1.0
						publish(client, nodeID, cabinTemp)
					}
				}
			}
		}

	}()
	<-done
}

// publish converts value to a string, and publishes it to the specified subTopic
func publish(client mqtt.Client, nodeID string, meas measurement) {
	strval := fmt.Sprintf("%f", meas.value)
	fullTopic := fmt.Sprintf("%s/i/%s", nodeID, meas.topic)
	fmt.Printf("Publishing %s: %s\n", fullTopic, strval)
	token := client.Publish(fullTopic, 1, true, strval)
	token.Wait()
}

// nextValue randomly increments or decrements `current` by a maximum value of `maxStep` and returns the result
func nextValue(current, maxStep float64) float64 {
	return current + (rnd.Float64()-0.5)*maxStep*2
}
